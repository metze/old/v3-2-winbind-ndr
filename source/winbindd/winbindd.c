/* 
   Unix SMB/CIFS implementation.

   Winbind daemon for ntdom nss module

   Copyright (C) by Tim Potter 2000-2002
   Copyright (C) Andrew Tridgell 2002
   Copyright (C) Jelmer Vernooij 2003
   Copyright (C) Volker Lendecke 2004
   Copyright (C) James Peach 2007
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "includes.h"
#include "winbindd.h"

#undef DBGC_CLASS
#define DBGC_CLASS DBGC_WINBIND

BOOL opt_nocache = False;

extern BOOL override_logfile;

struct event_context *winbind_event_context(void)
{
	static struct event_context *ctx;

	if (!ctx && !(ctx = event_context_init(NULL))) {
		smb_panic("Could not init winbind event context");
	}
	return ctx;
}

struct messaging_context *winbind_messaging_context(void)
{
	static struct messaging_context *ctx;

	if (!ctx && !(ctx = messaging_init(NULL, server_id_self(),
					   winbind_event_context()))) {
		smb_panic("Could not init winbind messaging context");
	}
	return ctx;
}

/* Reload configuration */

static BOOL reload_services_file(void)
{
	BOOL ret;

	if (lp_loaded()) {
		pstring fname;

		pstrcpy(fname,lp_configfile());
		if (file_exist(fname,NULL) && !strcsequal(fname,dyn_CONFIGFILE)) {
			pstrcpy(dyn_CONFIGFILE,fname);
		}
	}

	reopen_logs();
	ret = lp_load(dyn_CONFIGFILE,False,False,True,True);

	reopen_logs();
	load_interfaces();

	return(ret);
}


/**************************************************************************** **
 Handle a fault..
 **************************************************************************** */

static void fault_quit(void)
{
	dump_core();
}

static void winbindd_status(void)
{
	struct winbindd_cli_state *tmp;

	DEBUG(0, ("winbindd status:\n"));

	/* Print client state information */
	
	DEBUG(0, ("\t%d clients currently active\n", winbindd_num_clients()));
	
	if (DEBUGLEVEL >= 2 && winbindd_num_clients()) {
		DEBUG(2, ("\tclient list:\n"));
		for(tmp = winbindd_client_list(); tmp; tmp = tmp->next) {
			DEBUGADD(2, ("\t\tpid %lu, sock %d\n",
				  (unsigned long)tmp->pid, tmp->sock));
		}
	}
}

/* Print winbindd status to log file */

static void print_winbindd_status(void)
{
	winbindd_status();
}

/* Flush client cache */

static void flush_caches(void)
{
	/* We need to invalidate cached user list entries on a SIGHUP 
           otherwise cached access denied errors due to restrict anonymous
           hang around until the sequence number changes. */

	wcache_invalidate_cache();
}

/* Handle the signal by unlinking socket and exiting */

static void terminate(void)
{

	winbindd_release_sockets();
	idmap_close();
	
	trustdom_cache_shutdown();

#if 0
	if (interactive) {
		TALLOC_CTX *mem_ctx = talloc_init("end_description");
		char *description = talloc_describe_all(mem_ctx);

		DEBUG(3, ("tallocs left:\n%s\n", description));
		talloc_destroy(mem_ctx);
	}
#endif

	exit(0);
}

static BOOL do_sigterm;

static void termination_handler(int signum)
{
	do_sigterm = True;
	sys_select_signal(signum);
}

static BOOL do_sigusr2;

static void sigusr2_handler(int signum)
{
	do_sigusr2 = True;
	sys_select_signal(SIGUSR2);
}

static BOOL do_sighup;

static void sighup_handler(int signum)
{
	do_sighup = True;
	sys_select_signal(SIGHUP);
}

static BOOL do_sigchld;

static void sigchld_handler(int signum)
{
	do_sigchld = True;
	sys_select_signal(SIGCHLD);
}

/* React on 'smbcontrol winbindd reload-config' in the same way as on SIGHUP*/
static void msg_reload_services(struct messaging_context *msg,
				void *private_data,
				uint32_t msg_type,
				struct server_id server_id,
				DATA_BLOB *data)
{
        /* Flush various caches */
	flush_caches();
	reload_services_file();
}

/* React on 'smbcontrol winbindd shutdown' in the same way as on SIGTERM*/
static void msg_shutdown(struct messaging_context *msg,
			 void *private_data,
			 uint32_t msg_type,
			 struct server_id server_id,
			 DATA_BLOB *data)
{
	do_sigterm = True;
}


static void winbind_msg_validate_cache(struct messaging_context *msg_ctx,
				       void *private_data,
				       uint32_t msg_type,
				       struct server_id server_id,
				       DATA_BLOB *data)
{
	uint8 ret;
	pid_t child_pid;
	struct sigaction act;
	struct sigaction oldact;

	DEBUG(10, ("winbindd_msg_validate_cache: got validate-cache "
		   "message.\n"));

	/*
	 * call the validation code from a child:
	 * so we don't block the main winbindd and the validation
	 * code can safely use fork/waitpid...
	 */
	CatchChild();
	child_pid = sys_fork();

	if (child_pid == -1) {
		DEBUG(1, ("winbind_msg_validate_cache: Could not fork: %s\n",
			  strerror(errno)));
		return;
	}

	if (child_pid != 0) {
		/* parent */
		DEBUG(5, ("winbind_msg_validate_cache: child created with "
			  "pid %d.\n", child_pid));
		return;
	}

	/* child */

	/* install default SIGCHLD handler: validation code uses fork/waitpid */
	ZERO_STRUCT(act);
	act.sa_handler = SIG_DFL;
#ifdef SA_RESTART
	/* We *want* SIGALRM to interrupt a system call. */
	act.sa_flags = SA_RESTART;
#endif
	sigemptyset(&act.sa_mask);
	sigaddset(&act.sa_mask,SIGCHLD);
	sigaction(SIGCHLD,&act,&oldact);

	ret = (uint8)winbindd_validate_cache_nobackup();
	DEBUG(10, ("winbindd_msg_validata_cache: got return value %d\n", ret));
	messaging_send_buf(msg_ctx, server_id, MSG_WINBIND_VALIDATE_CACHE, &ret,
			   (size_t)1);
	_exit(0);
}

static struct winbindd_dispatch_table {
	enum winbindd_cmd cmd;
	void (*fn)(struct winbindd_cli_state *state);
	const char *winbindd_cmd_name;
} dispatch_table[] = {
	
	/* User functions */

	{ WINBINDD_GETPWNAM, winbindd_getpwnam, "GETPWNAM" },
	{ WINBINDD_GETPWUID, winbindd_getpwuid, "GETPWUID" },

	{ WINBINDD_SETPWENT, winbindd_setpwent, "SETPWENT" },
	{ WINBINDD_ENDPWENT, winbindd_endpwent, "ENDPWENT" },
	{ WINBINDD_GETPWENT, winbindd_getpwent, "GETPWENT" },

	{ WINBINDD_GETGROUPS, winbindd_getgroups, "GETGROUPS" },
	{ WINBINDD_GETUSERSIDS, winbindd_getusersids, "GETUSERSIDS" },
	{ WINBINDD_GETUSERDOMGROUPS, winbindd_getuserdomgroups,
	  "GETUSERDOMGROUPS" },

	/* Group functions */

	{ WINBINDD_GETGRNAM, winbindd_getgrnam, "GETGRNAM" },
	{ WINBINDD_GETGRGID, winbindd_getgrgid, "GETGRGID" },
	{ WINBINDD_SETGRENT, winbindd_setgrent, "SETGRENT" },
	{ WINBINDD_ENDGRENT, winbindd_endgrent, "ENDGRENT" },
	{ WINBINDD_GETGRENT, winbindd_getgrent, "GETGRENT" },
	{ WINBINDD_GETGRLST, winbindd_getgrent, "GETGRLST" },

	/* PAM auth functions */

	{ WINBINDD_PAM_AUTH, winbindd_pam_auth, "PAM_AUTH" },
	{ WINBINDD_PAM_AUTH_CRAP, winbindd_pam_auth_crap, "AUTH_CRAP" },
	{ WINBINDD_PAM_CHAUTHTOK, winbindd_pam_chauthtok, "CHAUTHTOK" },
	{ WINBINDD_PAM_LOGOFF, winbindd_pam_logoff, "PAM_LOGOFF" },
	{ WINBINDD_PAM_CHNG_PSWD_AUTH_CRAP, winbindd_pam_chng_pswd_auth_crap, "CHNG_PSWD_AUTH_CRAP" },

	/* Enumeration functions */

	{ WINBINDD_LIST_USERS, winbindd_list_users, "LIST_USERS" },
	{ WINBINDD_LIST_GROUPS, winbindd_list_groups, "LIST_GROUPS" },
	{ WINBINDD_LIST_TRUSTDOM, winbindd_list_trusted_domains,
	  "LIST_TRUSTDOM" },
	{ WINBINDD_SHOW_SEQUENCE, winbindd_show_sequence, "SHOW_SEQUENCE" },

	/* SID related functions */

	{ WINBINDD_LOOKUPSID, winbindd_lookupsid, "LOOKUPSID" },
	{ WINBINDD_LOOKUPNAME, winbindd_lookupname, "LOOKUPNAME" },
	{ WINBINDD_LOOKUPRIDS, winbindd_lookuprids, "LOOKUPRIDS" },

	/* Lookup related functions */

	{ WINBINDD_SID_TO_UID, winbindd_sid_to_uid, "SID_TO_UID" },
	{ WINBINDD_SID_TO_GID, winbindd_sid_to_gid, "SID_TO_GID" },
	{ WINBINDD_UID_TO_SID, winbindd_uid_to_sid, "UID_TO_SID" },
	{ WINBINDD_GID_TO_SID, winbindd_gid_to_sid, "GID_TO_SID" },
#if 0   /* DISABLED until we fix the interface in Samba 3.0.26 --jerry */
	{ WINBINDD_SIDS_TO_XIDS, winbindd_sids_to_unixids, "SIDS_TO_XIDS" },
#endif  /* end DISABLED */
	{ WINBINDD_ALLOCATE_UID, winbindd_allocate_uid, "ALLOCATE_UID" },
	{ WINBINDD_ALLOCATE_GID, winbindd_allocate_gid, "ALLOCATE_GID" },
	{ WINBINDD_SET_MAPPING, winbindd_set_mapping, "SET_MAPPING" },
	{ WINBINDD_SET_HWM, winbindd_set_hwm, "SET_HWMS" },

	/* Miscellaneous */

	{ WINBINDD_DUMP_MAPS, winbindd_dump_maps, "DUMP_MAPS" },

	{ WINBINDD_CHECK_MACHACC, winbindd_check_machine_acct, "CHECK_MACHACC" },
	{ WINBINDD_PING, winbindd_ping, "PING" },
	{ WINBINDD_INFO, winbindd_info, "INFO" },
	{ WINBINDD_INTERFACE_VERSION, winbindd_interface_version,
	  "INTERFACE_VERSION" },
	{ WINBINDD_DOMAIN_NAME, winbindd_domain_name, "DOMAIN_NAME" },
	{ WINBINDD_DOMAIN_INFO, winbindd_domain_info, "DOMAIN_INFO" },
	{ WINBINDD_NETBIOS_NAME, winbindd_netbios_name, "NETBIOS_NAME" },
	{ WINBINDD_PRIV_PIPE_DIR, winbindd_priv_pipe_dir,
	  "WINBINDD_PRIV_PIPE_DIR" },
	{ WINBINDD_GETDCNAME, winbindd_getdcname, "GETDCNAME" },
	{ WINBINDD_DSGETDCNAME, winbindd_dsgetdcname, "DSGETDCNAME" },

	/* Credential cache access */
	{ WINBINDD_CCACHE_NTLMAUTH, winbindd_ccache_ntlm_auth, "NTLMAUTH" },

	/* WINS functions */

	{ WINBINDD_WINS_BYNAME, winbindd_wins_byname, "WINS_BYNAME" },
	{ WINBINDD_WINS_BYIP, winbindd_wins_byip, "WINS_BYIP" },
	
	/* End of list */

	{ WINBINDD_NUM_CMDS, NULL, "NONE" }
};

static void process_struct_request(struct winbindd_cli_state *state)
{
	struct winbindd_dispatch_table *table = dispatch_table;

	/* Free response data - we may be interrupted and receive another
	   command before being able to send this data off. */

	SAFE_FREE(state->response.extra_data.data);  

	ZERO_STRUCT(state->response);

	state->response.result = WINBINDD_PENDING;
	state->response.length = sizeof(struct winbindd_response);

	/* Remember who asked us. */
	state->pid = state->request.pid;

	/* Process command */

	for (table = dispatch_table; table->fn; table++) {
		if (state->request.cmd == table->cmd) {
			DEBUG(10,("process_struct_request: request fn %s\n",
				  table->winbindd_cmd_name ));
			table->fn(state);
			break;
		}
	}

	if (!table->fn) {
		DEBUG(10,("process_struct_request: unknown request fn number %d\n",
			  (int)state->request.cmd ));
		request_error(state);
	}
}

static void winbindd_ndr_ping(struct winbindd_cli_state *state)
{
	struct winbind_ping *r;
	r = talloc_get_type_abort(state->c.ndr.r, struct winbind_ping);

	if (lp_parm_bool(-1, "winbindd", "ping_our_domain", False)) {
		sendto_domain(state, find_our_domain());
		return;
	}

	DEBUG(3, ("winbindd_ndr_ping()\n"));

	r->out.result = WINBIND_STATUS_OK;

	winbindd_reply_ndr_ok(state);
}

void winbindd_ndr_child_ping(struct winbindd_domain *domain,
			     struct winbindd_cli_state *state)
{
	struct winbind_ping *r;
	r = talloc_get_type_abort(state->c.ndr.r, struct winbind_ping);

	DEBUG(3, ("winbindd_ndr_child_ping()\n"));

	r->out.result = WINBIND_STATUS_OK;
}

static const struct winbind_ndr_cmd {
	uint32 opnum;
	void (*fn)(struct winbindd_cli_state *state);
} ndr_cmd_table[] = {
	{
		.opnum	= NDR_WINBIND_PING,
		.fn	= winbindd_ndr_ping
	}
};

static void request_reset(struct winbindd_cli_state *state, bool wait_for_request);

#define _WINBINDD_NOMEM(ptr) do { \
	if (!ptr) { \
		DEBUG(0,("%s: out of memory\n", __location__)); \
		request_reset(state, false); \
		return; \
	} \
} while (0)

#define _WINBINDD_CHECK_NTSTATUS(status) do { \
	if (!NT_STATUS_IS_OK(status)) { \
		DEBUG(0,("%s: status = %s\n", \
			__location__, \
			nt_errstr(status))); \
		request_reset(state, false); \
		return; \
	} \
} while (0)

#define WINBINDD_DEBUG_NDR_CALL(lvl, _c, _f, _n) do { \
	if (DEBUGLVL(lvl)) { \
		ndr_print_function_debug((ndr_print_function_t) \
					 (_c)->ndr.call->ndr_print, \
					 _n, \
					 _f, \
					 (_c)->ndr.r); \
	} \
} while (0)

NTSTATUS winbindd_pull_ndr_request(TALLOC_CTX *mem_ctx,
				   struct winbindd_ndr_call *c)
{
	struct ndr_pull *pull;
	NTSTATUS status;
	uint32 opnum;

	DEBUG(10,("winbindd_pull_ndr_request(): packet[%u]\n",
		c->in.packet.length));
	dump_data(10, c->in.packet.data, c->in.packet.length);

	pull = ndr_pull_init_blob(&c->in.packet, mem_ctx);
	NT_STATUS_HAVE_NO_MEMORY(pull);

	pull->flags |= LIBNDR_FLAG_REF_ALLOC;

	status = ndr_pull_STRUCT_winbind_header(pull,
						NDR_SCALARS|NDR_BUFFERS,
						&c->in.header);
	NT_STATUS_NOT_OK_RETURN(status);

	if (c->in.header.version != WINBIND_HEADER_VERSION) {
		DEBUG(0,("winbindd_pull_ndr_request: "
			 "Invalid protocol version: 0x%04X != 0x%04X\n",
			c->in.header.version, WINBIND_HEADER_VERSION));
		return NT_STATUS_INVALID_PARAMETER;
	}

	opnum = c->in.header.opnum;

	if (opnum >= ndr_table_winbind_protocol.num_calls) {
		DEBUG(10,("winbindd_pull_ndr_request: "
			  "unknown request opnum number 0x%04X >= 0x%04X\n",
			  opnum,
			  ndr_table_winbind_protocol.num_calls));
		return NT_STATUS_INVALID_PARAMETER;
	}

	DEBUG(3,("winbindd_pull_ndr_request: opnum[%u]\n", opnum));

	c->ndr.call	= &ndr_table_winbind_protocol.calls[opnum];
	c->ndr.r	= talloc_named(mem_ctx,
				       c->ndr.call->struct_size,
				       "struct %s",
				       c->ndr.call->name);
	NT_STATUS_HAVE_NO_MEMORY(c->ndr.r);

	status = c->ndr.call->ndr_pull(pull, NDR_IN, c->ndr.r);
	NT_STATUS_NOT_OK_RETURN(status);

	WINBINDD_DEBUG_NDR_CALL(10, c, NDR_IN, "pull_ndr_request");

	return NT_STATUS_OK;
}

NTSTATUS winbindd_pull_ndr_response(TALLOC_CTX *mem_ctx,
				    struct winbindd_ndr_call *c)
{
	struct ndr_pull *pull;
	NTSTATUS status;
	uint32 opnum;
	const struct ndr_interface_call *call;

	DEBUG(10,("winbindd_pull_ndr_response(): packet[%u]\n",
		c->out.packet.length));
	dump_data(10, c->out.packet.data, c->out.packet.length);

	pull = ndr_pull_init_blob(&c->out.packet, mem_ctx);
	NT_STATUS_HAVE_NO_MEMORY(pull);

	pull->flags |= LIBNDR_FLAG_REF_ALLOC;

	status = ndr_pull_STRUCT_winbind_header(pull,
						NDR_SCALARS|NDR_BUFFERS,
						&c->out.header);
	NT_STATUS_NOT_OK_RETURN(status);

	if (c->out.header.version != WINBIND_HEADER_VERSION) {
		DEBUG(0,("winbindd_pull_ndr_response: "
			 "Invalid protocol version: 0x%04X != 0x%04X\n",
			 c->out.header.version, WINBIND_HEADER_VERSION));
		return NT_STATUS_INVALID_PARAMETER;
	}

	if (!(c->out.header.flags & WINBIND_HEADER_FLAGS_RESPONSE)) {
		DEBUG(0,("winbindd_pull_ndr_response: "
			 "Flags[0x%08X] miss WINBIND_HEADER_FLAGS_RESPONSE [0x%08X]\n",
			 c->out.header.flags, WINBIND_HEADER_FLAGS_RESPONSE));
		return NT_STATUS_INVALID_PARAMETER;
	}

	opnum = c->out.header.opnum;

	if (opnum >= ndr_table_winbind_protocol.num_calls) {
		DEBUG(10,("winbindd_pull_ndr_response: "
			  "unknown request opnum number 0x%04X >= 0x%04X\n",
			  opnum,
			  ndr_table_winbind_protocol.num_calls));
		return NT_STATUS_INVALID_PARAMETER;
	}

	DEBUG(3,("winbindd_pull_ndr_response: opnum[%u]\n", opnum));

	call = &ndr_table_winbind_protocol.calls[opnum];

	if (c->ndr.call	!= call) {
		DEBUG(10,("winbindd_pull_ndr_response: "
			  "invalid response [%s] - expected [%s]\n",
			  call->name, c->ndr.call->name));
		return NT_STATUS_INVALID_PARAMETER;
	}

	if (c->out.header.flags & WINBIND_HEADER_FLAGS_ERROR) {
		status = ndr_pull_winbind_status(pull,
						 NDR_SCALARS|NDR_BUFFERS,
						 &c->ndr.result);
		NT_STATUS_NOT_OK_RETURN(status);
	} else {
		status = c->ndr.call->ndr_pull(pull, NDR_OUT, c->ndr.r);
		NT_STATUS_NOT_OK_RETURN(status);
		c->ndr.result = WINBIND_STATUS_OK;

		WINBINDD_DEBUG_NDR_CALL(10, c, NDR_OUT, "pull_ndr_response");
	}

	return NT_STATUS_OK;
}

static void process_ndr_request(struct winbindd_cli_state *state)
{
	NTSTATUS status;
	uint32 i;

	status = winbindd_pull_ndr_request(state->mem_ctx, &state->c);
	_WINBINDD_CHECK_NTSTATUS(status);

	for (i=0; i < ARRAY_SIZE(ndr_cmd_table); i++) {
		if (ndr_cmd_table[i].opnum == state->c.in.header.opnum) {
			ndr_cmd_table[i].fn(state);
			return;
		}
	}

	winbindd_reply_ndr_error(state, WINBIND_STATUS_NOT_IMPLEMENTED);
}

static void process_request(struct winbindd_cli_state *state)
{
	if (state->is_ndr) {
		process_ndr_request(state);
		return;
	}

	process_struct_request(state);
}

/*
 * A list of file descriptors being monitored by select in the main processing
 * loop. fd_event->handler is called whenever the socket is readable/writable.
 */

static struct fd_event *fd_events = NULL;

void add_fd_event(struct fd_event *ev)
{
	struct fd_event *match;

	/* only add unique fd_event structs */

	for (match=fd_events; match; match=match->next ) {
#ifdef DEVELOPER
		SMB_ASSERT( match != ev );
#else
		if ( match == ev )
			return;
#endif
	}

	DLIST_ADD(fd_events, ev);
}

void remove_fd_event(struct fd_event *ev)
{
	DLIST_REMOVE(fd_events, ev);
}

/*
 * Handler for fd_events to complete a read/write request, set up by
 * setup_async_read/setup_async_write.
 */

static void rw_callback(struct fd_event *event, int flags)
{
	size_t todo;
	ssize_t done = 0;

	todo = event->length - event->done;

	if (event->flags & EVENT_FD_WRITE) {
		SMB_ASSERT(flags == EVENT_FD_WRITE);
		done = sys_write(event->fd,
				 &((char *)event->data)[event->done],
				 todo);

		if (done <= 0) {
			event->flags = 0;
			event->finished(event->private_data, False);
			return;
		}
	}

	if (event->flags & EVENT_FD_READ) {
		SMB_ASSERT(flags == EVENT_FD_READ);
		done = sys_read(event->fd, &((char *)event->data)[event->done],
				todo);

		if (done <= 0) {
			event->flags = 0;
			event->finished(event->private_data, False);
			return;
		}
	}

	event->done += done;

	if (event->done == event->length) {
		event->flags = 0;
		event->finished(event->private_data, True);
	}
}

/*
 * Request an async read/write on a fd_event structure. (*finished) is called
 * when the request is completed or an error had occurred.
 */

void setup_async_read(struct fd_event *event, void *data, size_t length,
		      void (*finished)(void *private_data, BOOL success),
		      void *private_data)
{
	SMB_ASSERT(event->flags == 0);
	event->data = data;
	event->length = length;
	event->done = 0;
	event->handler = rw_callback;
	event->finished = finished;
	event->private_data = private_data;
	event->flags = EVENT_FD_READ;
}

void setup_async_write(struct fd_event *event, void *data, size_t length,
		       void (*finished)(void *private_data, BOOL success),
		       void *private_data)
{
	SMB_ASSERT(event->flags == 0);
	event->data = data;
	event->length = length;
	event->done = 0;
	event->handler = rw_callback;
	event->finished = finished;
	event->private_data = private_data;
	event->flags = EVENT_FD_WRITE;
}

/*
 * This is the main event loop of winbind requests. It goes through a
 * state-machine of 3 read/write requests, 4 if you have extra data to send.
 *

 * An idle winbind client has a read request of 8 bytes outstanding,
 * finalizing function is request_autodetect_recv, which autodetects
 * the used protocol. Then it triggers a read request for the remaining
 * bytes to complete the request PDU. For the struct based protocol
 * maybe two reads are needed it the request contains extra data. The
 * NDR based protocol needs only one read request.
 *
 * NOTE: the protocol version is per request and not per client connection
 *       so the client can mix the old and new requests.
 *
 * The process_request() function are calls process_[struct|ndr]_request()
 * to dispatch the request to the implementation function
 * depending on the autodetection.
 *
 * Struct based requests are finished by calling
 * request_error() or request_ok().
 *
 * NDR bases requests are finished by calling
 * winbindd_reply_ndr_error() or winbindd_reply_ndr_ok()
 */

static void request_autodetect_recv(void *private_data, BOOL success);
static void request_recv(void *private_data, BOOL success);
static void request_main_recv(void *private_data, BOOL success);
static void request_finished(struct winbindd_cli_state *state);
void request_finished_cont(void *private_data, BOOL success);
static void response_main_sent(void *private_data, BOOL success);
static void response_extra_sent(void *private_data, BOOL success);

static void request_reset(struct winbindd_cli_state *state, bool wait_for_request)
{
	TALLOC_FREE(state->mem_ctx);

	SAFE_FREE(state->request.extra_data.data);
	SAFE_FREE(state->response.extra_data.data);

	ZERO_STRUCT(state->request);
	ZERO_STRUCT(state->response);

	ZERO_STRUCT(state->c);

	if (!wait_for_request) {
		state->finished = True;
		return;
	}

	setup_async_read(&state->fd_event,
			 state->c.autodetect,
			 sizeof(state->c.autodetect),
			 request_autodetect_recv, state);
}

static void response_extra_sent(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		request_reset(state, false);
		return;
	}

	request_reset(state, true);
}

static void response_main_sent(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		request_reset(state, false);
		return;
	}

	if (state->response.length == sizeof(state->response)) {
		request_reset(state, true);
		return;
	}

	setup_async_write(&state->fd_event, state->response.extra_data.data,
			  state->response.length - sizeof(state->response),
			  response_extra_sent, state);
}

static void request_finished(struct winbindd_cli_state *state)
{
	setup_async_write(&state->fd_event, &state->response,
			  sizeof(state->response), response_main_sent, state);
}

void request_error(struct winbindd_cli_state *state)
{
	SMB_ASSERT(state->response.result == WINBINDD_PENDING);
	state->response.result = WINBINDD_ERROR;
	request_finished(state);
}

void request_ok(struct winbindd_cli_state *state)
{
	SMB_ASSERT(state->response.result == WINBINDD_PENDING);
	state->response.result = WINBINDD_OK;
	request_finished(state);
}

void request_finished_cont(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (success)
		request_ok(state);
	else
		request_error(state);
}

NTSTATUS winbindd_push_ndr_response(TALLOC_CTX *mem_ctx,
				    struct winbindd_ndr_call *c)
{
	struct ndr_push *push;
	NTSTATUS status;

	push = ndr_push_init_ctx(mem_ctx);
	NT_STATUS_HAVE_NO_MEMORY(push);

	c->out.header		= c->in.header;
	c->out.header.length	= 0; /* will be overwritten later */
	c->out.header.flags	= WINBIND_HEADER_FLAGS_RESPONSE;

	if (c->ndr.result != WINBIND_STATUS_OK) {
		c->out.header.flags |= WINBIND_HEADER_FLAGS_ERROR;
	}

	status = ndr_push_STRUCT_winbind_header(push,
						 NDR_SCALARS|NDR_BUFFERS,
						 &c->out.header);
	NT_STATUS_NOT_OK_RETURN(status);

	/* only push the payload if the result is ok */
	if (c->ndr.result == WINBIND_STATUS_OK) {
		WINBINDD_DEBUG_NDR_CALL(10, c, NDR_OUT, "push_ndr_response");

		status = c->ndr.call->ndr_push(push,
					       NDR_OUT,
					       c->ndr.r);
		NT_STATUS_NOT_OK_RETURN(status);
	} else {
		status = ndr_push_winbind_status(push,
						  NDR_SCALARS|NDR_BUFFERS,
						  c->ndr.result);
		NT_STATUS_NOT_OK_RETURN(status);
	}

	c->out.packet = ndr_push_blob(push);

	/* fix the length */
	RSIVAL(c->out.packet.data, 0,
	       c->out.packet.length - 4);

	DEBUG(10,("winbindd_push_ndr_response(): packet[%u]\n",
		c->out.packet.length));
	dump_data(10, c->out.packet.data, c->out.packet.length);

	return NT_STATUS_OK;
}

NTSTATUS winbindd_push_ndr_request(TALLOC_CTX *mem_ctx,
				   struct winbindd_ndr_call *c)
{
	struct ndr_push *push;
	NTSTATUS status;

	WINBINDD_DEBUG_NDR_CALL(10, c, NDR_IN, "push_ndr_request");

#define _GET_OPNUM(c) (c->ndr.call - ndr_table_winbind_protocol.calls)
	ZERO_STRUCT(c->in.header);
	c->in.header.opnum = _GET_OPNUM(c);

	push = ndr_push_init_ctx(mem_ctx);
	NT_STATUS_HAVE_NO_MEMORY(push);

	status = ndr_push_STRUCT_winbind_header(push,
						NDR_SCALARS|NDR_BUFFERS,
						&c->in.header);
	NT_STATUS_NOT_OK_RETURN(status);

	status = c->ndr.call->ndr_push(push,
				       NDR_IN,
				       c->ndr.r);
	NT_STATUS_NOT_OK_RETURN(status);

	c->in.packet = ndr_push_blob(push);

	/* fix the length */
	RSIVAL(c->in.packet.data, 0,
	       c->in.packet.length - 4);

	DEBUG(10,("winbindd_push_ndr_request(): packet[%u]\n",
		c->in.packet.length));
	dump_data(10, c->in.packet.data, c->in.packet.length);

	return NT_STATUS_OK;
}

void winbindd_reply_ndr_error(struct winbindd_cli_state *state,
			      enum winbind_status result)
{
	NTSTATUS status;

	state->c.ndr.result = result;

	status = winbindd_push_ndr_response(state->mem_ctx,
					    &state->c);
	_WINBINDD_CHECK_NTSTATUS(status);

	setup_async_write(&state->fd_event,
			  state->c.out.packet.data,
			  state->c.out.packet.length,
			  response_extra_sent, state);
}

void winbindd_reply_ndr_ok(struct winbindd_cli_state *state)
{
	winbindd_reply_ndr_error(state, WINBIND_STATUS_OK);
}

NTSTATUS winbindd_ndr_parse_autodetect(TALLOC_CTX *mem_ctx,
				       struct winbindd_ndr_call *c,
				       uint32 max_length,
				       BOOL in)
{
	uint32 min_length = WINBIND_HEADER_SIZE - 4;
	uint32 length;
	DATA_BLOB *p;

	length = RIVAL(c->autodetect, 0);

	if (strncmp("WBPT", (const char *)&c->autodetect[4], 4) != 0) {
		DEBUG(0,("request_header_ndr_recv: "
			 "Invalid header magic: 0x%08X\n",
			 RIVAL(c->autodetect, 4)));
		return NT_STATUS_INVALID_PARAMETER;
	}

	if (length < min_length) {
		DEBUG(0,("request_header_ndr_recv: "
			 "Invalid request size received: %d < %d\n",
			 length, min_length));
		return NT_STATUS_INVALID_PARAMETER;
	}

	if (length > max_length) {
		DEBUG(0,("request_header_ndr_recv: "
			 "Invalid request size received: 0x%08X > 0x%08X\n",
			 length, max_length));
		return NT_STATUS_INVALID_PARAMETER;
	}

	if (in) {
		p = &c->in.packet;
	} else {
		p = &c->out.packet;
	}

	*p = data_blob_talloc(mem_ctx, NULL, length + 4);
	if (!p->data) {
		DEBUG(0,("request_header_ndr_recv: out of memory (0x%08X)\n",
			length + 4));
		return NT_STATUS_NO_MEMORY;
	}

	memcpy(p->data,
	       c->autodetect,
	       sizeof(c->autodetect));

	return NT_STATUS_OK;
}

static void request_ndr_recv(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	NTSTATUS status;
	uint32 max_length;
	size_t autodetect_len = sizeof(state->c.autodetect);

	if (!success) {
		request_reset(state, false);
		return;
	}

	if (state->privileged) {
		max_length = WINBIND_MAX_LENGTH_PRIVILEGED;
	} else {
		max_length = WINBIND_MAX_LENGTH_UNPRIVILEGED;
	}

	status = winbindd_ndr_parse_autodetect(state->mem_ctx,
					       &state->c,
					       max_length,
					       True);
	if (!NT_STATUS_IS_OK(status)) {
		state->finished = True;
		return;
	}

	state->is_ndr = true;
	setup_async_read(&state->fd_event,
			 state->c.in.packet.data + autodetect_len,
			 state->c.in.packet.length - autodetect_len,
			 request_recv, state);
}

static void request_struct_recv(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);
	size_t autodetect_len = sizeof(state->c.autodetect);

	if (!success) {
		state->finished = True;
		return;
	}

	memcpy(&state->request,
	       state->c.autodetect,
	       autodetect_len);

	if (*(uint32 *)(&state->request) != sizeof(state->request)) {
		DEBUG(0,("request_struct_recv: Invalid request size received: %d\n",
			 *(uint32 *)(&state->request)));
		state->finished = True;
		return;
	}

	state->is_ndr = false;
	setup_async_read(&state->fd_event,
			 ((uint8 *)(&state->request)) + autodetect_len,
			 sizeof(state->request) - autodetect_len,
			 request_main_recv, state);
}


static void request_autodetect_recv(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		state->finished = True;
		return;
	}

	state->mem_ctx = talloc_new(state);
	if (!state->mem_ctx) {
		state->finished = True;
		return;
	}

	if (strncmp("WBPT", (const char *)&state->c.autodetect[4], 4) == 0) {
		request_ndr_recv(private_data, success);
		return;
	}

	request_struct_recv(private_data, success);
}

static void request_main_recv(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		state->finished = True;
		return;
	}

	if (state->request.extra_len == 0) {
		state->request.extra_data.data = NULL;
		request_recv(state, True);
		return;
	}

	if ((!state->privileged) &&
	    (state->request.extra_len > WINBINDD_MAX_EXTRA_DATA)) {
		DEBUG(3, ("Got request with %d bytes extra data on "
			  "unprivileged socket\n", (int)state->request.extra_len));
		state->request.extra_data.data = NULL;
		state->finished = True;
		return;
	}

	state->request.extra_data.data =
		SMB_MALLOC_ARRAY(char, state->request.extra_len + 1);

	if (state->request.extra_data.data == NULL) {
		DEBUG(0, ("malloc failed\n"));
		state->finished = True;
		return;
	}

	/* Ensure null termination */
	state->request.extra_data.data[state->request.extra_len] = '\0';

	setup_async_read(&state->fd_event, state->request.extra_data.data,
			 state->request.extra_len, request_recv, state);
}

static void request_recv(void *private_data, BOOL success)
{
	struct winbindd_cli_state *state =
		talloc_get_type_abort(private_data, struct winbindd_cli_state);

	if (!success) {
		state->finished = True;
		return;
	}

	process_request(state);
}

/* Process a new connection by adding it to the client connection list */

static void new_connection(int listen_sock, BOOL privileged)
{
	struct sockaddr_un sunaddr;
	struct winbindd_cli_state *state;
	socklen_t len;
	int sock;
	
	/* Accept connection */
	
	len = sizeof(sunaddr);

	do {
		sock = accept(listen_sock, (struct sockaddr *)&sunaddr, &len);
	} while (sock == -1 && errno == EINTR);

	if (sock == -1)
		return;
	
	DEBUG(6,("accepted socket %d\n", sock));
	
	/* Create new connection structure */
	
	if ((state = TALLOC_ZERO_P(NULL, struct winbindd_cli_state)) == NULL) {
		close(sock);
		return;
	}
	
	state->sock = sock;

	state->last_access = time(NULL);	

	state->privileged = privileged;

	state->fd_event.fd = state->sock;
	state->fd_event.flags = 0;
	add_fd_event(&state->fd_event);

	setup_async_read(&state->fd_event,
			 state->c.autodetect,
			 sizeof(state->c.autodetect),
			 request_autodetect_recv, state);

	/* Add to connection list */
	
	winbindd_add_client(state);
}

/* Remove a client connection from client connection list */

static void remove_client(struct winbindd_cli_state *state)
{
	/* It's a dead client - hold a funeral */
	
	if (state == NULL) {
		return;
	}
		
	/* Close socket */
		
	close(state->sock);
		
	/* Free any getent state */
		
	free_getent_state(state->getpwent_state);
	free_getent_state(state->getgrent_state);
		
	/* We may have some extra data that was not freed if the client was
	   killed unexpectedly */

	request_reset(state, false);

	remove_fd_event(&state->fd_event);
		
	/* Remove from list and free */
		
	winbindd_remove_client(state);
	TALLOC_FREE(state);
}

/* Shutdown client connection which has been idle for the longest time */

static BOOL remove_idle_client(void)
{
	struct winbindd_cli_state *state, *remove_state = NULL;
	time_t last_access = 0;
	int nidle = 0;

	for (state = winbindd_client_list(); state; state = state->next) {
		if (state->response.result != WINBINDD_PENDING &&
		    !state->getpwent_state && !state->getgrent_state) {
			nidle++;
			if (!last_access || state->last_access < last_access) {
				last_access = state->last_access;
				remove_state = state;
			}
		}
	}

	if (remove_state) {
		DEBUG(5,("Found %d idle client connections, shutting down sock %d, pid %u\n",
			nidle, remove_state->sock, (unsigned int)remove_state->pid));
		remove_client(remove_state);
		return True;
	}

	return False;
}

/* Process incoming clients on listen_sock.  We use a tricky non-blocking,
   non-forking, non-threaded model which allows us to handle many
   simultaneous connections while remaining impervious to many denial of
   service attacks. */

static int process_loop(int listen_sock, int listen_priv_sock)
{
	struct winbindd_cli_state *state;
	struct fd_event *ev;
	fd_set r_fds, w_fds;
	int maxfd, selret;
	struct timeval timeout, ev_timeout;

	/* We'll be doing this a lot */

	/* Handle messages */

	message_dispatch(winbind_messaging_context());

	run_events(winbind_event_context(), 0, NULL, NULL);

	/* refresh the trusted domain cache */

	rescan_trusted_domains();

	/* Initialise fd lists for select() */

	maxfd = MAX(listen_sock, listen_priv_sock);

	FD_ZERO(&r_fds);
	FD_ZERO(&w_fds);
	FD_SET(listen_sock, &r_fds);
	FD_SET(listen_priv_sock, &r_fds);

	timeout.tv_sec = WINBINDD_ESTABLISH_LOOP;
	timeout.tv_usec = 0;

	/* Check for any event timeouts. */
	if (get_timed_events_timeout(winbind_event_context(), &ev_timeout)) {
		timeout = timeval_min(&timeout, &ev_timeout);
	}

	/* Set up client readers and writers */

	state = winbindd_client_list();

	while (state) {

		struct winbindd_cli_state *next = state->next;

		/* Dispose of client connection if it is marked as 
		   finished */ 

		if (state->finished)
			remove_client(state);

		state = next;
	}

	for (ev = fd_events; ev; ev = ev->next) {
		if (ev->flags & EVENT_FD_READ) {
			FD_SET(ev->fd, &r_fds);
			maxfd = MAX(ev->fd, maxfd);
		}
		if (ev->flags & EVENT_FD_WRITE) {
			FD_SET(ev->fd, &w_fds);
			maxfd = MAX(ev->fd, maxfd);
		}
	}

	/* Call select */
        
	selret = sys_select(maxfd + 1, &r_fds, &w_fds, NULL, &timeout);

	if (selret == 0) {
		goto no_fds_ready;
	}

	if (selret == -1) {
		if (errno == EINTR) {
			goto no_fds_ready;
		}

		/* Select error, something is badly wrong */

		perror("select");
		exit(1);
	}

	/* selret > 0 */

	ev = fd_events;
	while (ev != NULL) {
		struct fd_event *next = ev->next;
		int flags = 0;
		if (FD_ISSET(ev->fd, &r_fds))
			flags |= EVENT_FD_READ;
		if (FD_ISSET(ev->fd, &w_fds))
			flags |= EVENT_FD_WRITE;
		if (flags)
			ev->handler(ev, flags);
		ev = next;
	}

	if (FD_ISSET(listen_sock, &r_fds)) {
		while (winbindd_num_clients() >
		       WINBINDD_MAX_SIMULTANEOUS_CLIENTS - 1) {
			DEBUG(5,("winbindd: Exceeding %d client "
				 "connections, removing idle "
				 "connection.\n",
				 WINBINDD_MAX_SIMULTANEOUS_CLIENTS));
			if (!remove_idle_client()) {
				DEBUG(0,("winbindd: Exceeding %d "
					 "client connections, no idle "
					 "connection found\n",
					 WINBINDD_MAX_SIMULTANEOUS_CLIENTS));
				break;
			}
		}
		/* new, non-privileged connection */
		new_connection(listen_sock, False);
	}
            
	if (FD_ISSET(listen_priv_sock, &r_fds)) {
		while (winbindd_num_clients() >
		       WINBINDD_MAX_SIMULTANEOUS_CLIENTS - 1) {
			DEBUG(5,("winbindd: Exceeding %d client "
				 "connections, removing idle "
				 "connection.\n",
				 WINBINDD_MAX_SIMULTANEOUS_CLIENTS));
			if (!remove_idle_client()) {
				DEBUG(0,("winbindd: Exceeding %d "
					 "client connections, no idle "
					 "connection found\n",
					 WINBINDD_MAX_SIMULTANEOUS_CLIENTS));
				break;
			}
		}
		/* new, privileged connection */
		new_connection(listen_priv_sock, True);
	}

 no_fds_ready:

#if 0
	winbindd_check_cache_size(time(NULL));
#endif

	/* Check signal handling things */

	if (do_sigterm)
		terminate();

	if (do_sighup) {

		DEBUG(3, ("got SIGHUP\n"));

		flush_caches();
		reload_services_file();

		do_sighup = False;
	}

	if (do_sigusr2) {
		print_winbindd_status();
		do_sigusr2 = False;
	}

	if (do_sigchld) {
		pid_t pid;

		do_sigchld = False;

		while ((pid = sys_waitpid(-1, NULL, WNOHANG)) > 0) {
			winbind_child_died(pid);
		}
	}


	return winbindd_num_clients();
}

static void winbindd_process_loop(enum smb_server_mode server_mode)
{
	int idle_timeout_sec;
	struct timeval starttime;
	int listen_public, listen_priv;

	errno = 0;
	if (!winbindd_init_sockets(&listen_public, &listen_priv,
				    &idle_timeout_sec)) {
		terminate();
	}

	starttime = timeval_current();

	if (listen_public == -1 || listen_priv == -1) {
		DEBUG(0, ("failed to open winbindd pipes: %s\n",
			    errno ? strerror(errno) : "unknown error"));
		terminate();
	}

	for (;;) {
		TALLOC_CTX *frame = talloc_stackframe();
		int clients = process_loop(listen_public, listen_priv);

		/* Don't bother figuring out the idle time if we won't be
		 * timing out anyway.
		 */
		if (idle_timeout_sec < 0) {
			TALLOC_FREE(frame);
			continue;
		}

		if (clients == 0 && server_mode == SERVER_MODE_FOREGROUND) {
			struct timeval now;

			now = timeval_current();
			if (timeval_elapsed2(&starttime, &now) >
				(double)idle_timeout_sec) {
				DEBUG(0, ("idle for %d secs, exitting\n",
					    idle_timeout_sec));
				terminate();
			}
		} else {
			starttime = timeval_current();
		}
		TALLOC_FREE(frame);
	}
}

/* Main function */

int main(int argc, char **argv, char **envp)
{
	pstring logfile;
	BOOL log_stdout = False;
	BOOL no_process_group = False;

	enum smb_server_mode server_mode = SERVER_MODE_DAEMON;

	struct poptOption long_options[] = {
		POPT_AUTOHELP
		{ "stdout", 'S', POPT_ARG_VAL, &log_stdout, True, "Log to stdout" },
		{ "foreground", 'F', POPT_ARG_VAL, &server_mode, SERVER_MODE_FOREGROUND, "Daemon in foreground mode" },
		{ "no-process-group", 0, POPT_ARG_VAL, &no_process_group, True, "Don't create a new process group" },
		{ "daemon", 'D', POPT_ARG_VAL, &server_mode, SERVER_MODE_DAEMON, "Become a daemon (default)" },
		{ "interactive", 'i', POPT_ARG_VAL, &server_mode, SERVER_MODE_INTERACTIVE, "Interactive mode" },
		{ "no-caching", 'n', POPT_ARG_VAL, &opt_nocache, True, "Disable caching" },
		POPT_COMMON_SAMBA
		POPT_TABLEEND
	};
	poptContext pc;
	int opt;

	/* glibc (?) likes to print "User defined signal 1" and exit if a
	   SIGUSR[12] is received before a handler is installed */

 	CatchSignal(SIGUSR1, SIG_IGN);
 	CatchSignal(SIGUSR2, SIG_IGN);

	fault_setup((void (*)(void *))fault_quit );
	dump_core_setup("winbindd");

	load_case_tables();

	/* Initialise for running in non-root mode */

	sec_init();

	set_remote_machine_name("winbindd", False);

	/* Set environment variable so we don't recursively call ourselves.
	   This may also be useful interactively. */

	if ( !winbind_off() ) {
		DEBUG(0,("Failed to disable recusive winbindd calls.  Exiting.\n"));
		exit(1);
	}

	/* Initialise samba/rpc client stuff */

	pc = poptGetContext("winbindd", argc, (const char **)argv, long_options, 0);

	while ((opt = poptGetNextOpt(pc)) != -1) {
		switch (opt) {
		default:
			d_fprintf(stderr, "\nInvalid option %s: %s\n\n",
				  poptBadOption(pc, 0), poptStrerror(opt));
			poptPrintUsage(pc, stderr, 0);
			exit(1);
		}
	}

	if (server_mode == SERVER_MODE_INTERACTIVE) {
		log_stdout = True;
		if (DEBUGLEVEL >= 9) {
			talloc_enable_leak_report();
		}
	}

	if (log_stdout && server_mode == SERVER_MODE_DAEMON) {
		printf("Can't log to stdout (-S) unless daemon is in foreground (-F) or interactive (-i)\n");
		poptPrintUsage(pc, stderr, 0);
		exit(1);
	}

	poptFreeContext(pc);

	if (!override_logfile) {
		pstr_sprintf(logfile, "%s/log.winbindd", dyn_LOGFILEBASE);
		lp_set_logfile(logfile);
	}
	setup_logging("winbindd", log_stdout);
	reopen_logs();

	DEBUG(1, ("winbindd version %s started.\n%s\n", 
		  SAMBA_VERSION_STRING, 
		  COPYRIGHT_STARTUP_MESSAGE) );

	if (!reload_services_file()) {
		DEBUG(0, ("error opening config file\n"));
		exit(1);
	}

	if (!directory_exist(lp_lockdir(), NULL)) {
		mkdir(lp_lockdir(), 0755);
	}

	/* Setup names. */

	if (!init_names())
		exit(1);

  	load_interfaces();

	if (!secrets_init()) {

		DEBUG(0,("Could not initialize domain trust account secrets. Giving up\n"));
		return False;
	}

	/* Enable netbios namecache */

	namecache_enable();

	/* Winbind daemon initialisation */

	if ( ! NT_STATUS_IS_OK(idmap_init_cache()) ) {
		DEBUG(1, ("Could not init idmap cache!\n"));		
	}

	/* Unblock all signals we are interested in as they may have been
	   blocked by the parent process. */

	BlockSignals(False, SIGINT);
	BlockSignals(False, SIGQUIT);
	BlockSignals(False, SIGTERM);
	BlockSignals(False, SIGUSR1);
	BlockSignals(False, SIGUSR2);
	BlockSignals(False, SIGHUP);
	BlockSignals(False, SIGCHLD);

	/* Setup signal handlers */
	
	CatchSignal(SIGINT, termination_handler);      /* Exit on these sigs */
	CatchSignal(SIGQUIT, termination_handler);
	CatchSignal(SIGTERM, termination_handler);
	CatchSignal(SIGCHLD, sigchld_handler);

	CatchSignal(SIGPIPE, SIG_IGN);                 /* Ignore sigpipe */

	CatchSignal(SIGUSR2, sigusr2_handler);         /* Debugging sigs */
	CatchSignal(SIGHUP, sighup_handler);

	if (server_mode == SERVER_MODE_DAEMON) {
		DEBUG( 3, ( "Becoming a daemon.\n" ) );
		become_daemon(True, no_process_group);
	} else if (server_mode == SERVER_MODE_FOREGROUND) {
		become_daemon(False, no_process_group);
	}

	pidfile_create("winbindd");

	/* Ensure all cache and idmap caches are consistent
	   before we startup. */

	if (winbindd_validate_cache()) {
		/* We have a bad cache, but luckily we
		   just deleted it. Restart ourselves */
		int i;
		/* Ensure we have no open low fd's. */
		for (i = 3; i < 100; i++) {
			close(i);
		}
		return execve(argv[0], argv, envp);
	}

#if HAVE_SETPGID
	/*
	 * If we're interactive we want to set our own process group for
	 * signal management.
	 */
	if (server_mode == SERVER_MODE_INTERACTIVE && !no_process_group) {
		setpgid( (pid_t)0, (pid_t)0);
	}
#endif

	TimeInit();

	/* Initialise messaging system */

	if (winbind_messaging_context() == NULL) {
		DEBUG(0, ("unable to initialize messaging system\n"));
		exit(1);
	}
	
	/* Initialize cache (ensure version is correct). */
	if (!initialize_winbindd_cache()) {
		exit(1);
	}

	/* React on 'smbcontrol winbindd reload-config' in the same way
	   as to SIGHUP signal */
	messaging_register(winbind_messaging_context(), NULL,
			   MSG_SMB_CONF_UPDATED, msg_reload_services);
	messaging_register(winbind_messaging_context(), NULL,
			   MSG_SHUTDOWN, msg_shutdown);

	/* Handle online/offline messages. */
	messaging_register(winbind_messaging_context(), NULL,
			   MSG_WINBIND_OFFLINE, winbind_msg_offline);
	messaging_register(winbind_messaging_context(), NULL,
			   MSG_WINBIND_ONLINE, winbind_msg_online);
	messaging_register(winbind_messaging_context(), NULL,
			   MSG_WINBIND_ONLINESTATUS, winbind_msg_onlinestatus);

	messaging_register(winbind_messaging_context(), NULL,
			   MSG_DUMP_EVENT_LIST, winbind_msg_dump_event_list);

	messaging_register(winbind_messaging_context(), NULL,
			   MSG_WINBIND_VALIDATE_CACHE,
			   winbind_msg_validate_cache);

	netsamlogon_cache_init(); /* Non-critical */
	
	/* clear the cached list of trusted domains */

	wcache_tdc_clear();	
	
	if (!init_domain_list()) {
		DEBUG(0,("unable to initalize domain list\n"));
		exit(1);
	}

	init_idmap_child();
	init_locator_child();

	smb_nscd_flush_user_cache();
	smb_nscd_flush_group_cache();

	/* Loop waiting for requests */
	winbindd_process_loop(server_mode);

	return 0;
}
